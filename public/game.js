// I prefer a class for future expandability.
class simonGame{
  // the class constructor
  constructor(){
    // this should load a high score from localStorage.
    // TODO: load from LocalStroage
    this.highScore = 0;
    // a list of colors for the game.
    this.colorsEnum = {
      GREEN: {css:"#Green", target:"Green", tone:329.6275569128699},
      YELLOW: {css:"#Yellow", target:"Yellow", tone:349.2282314330039},
      RED: {css:"#Red", target:"Red", tone:391.99543598174927},
      BLUE: {css:"#Blue", target:"Blue", tone:440}
    };
    // this is for the random picker. (is this needed?)
    this.colorsPointers = [
      this.colorsEnum.GREEN,
      this.colorsEnum.YELLOW,
      this.colorsEnum.RED,
      this.colorsEnum.BLUE,
    ]
    //the color sequence.
    this.colorSeq = [];
    this.onNum = 0;
    // the available status for the game.
    this.statusEnum = {
      OFF: "Game Over.",
      WATCH: "Watch...",
      REPEAT: "Your turn.",
      CORRECT: "Correct!",
    };
    // the current status
    this.status = this.statusEnum.OFF;
    this.playing = false;
    this.tone = null;
    // adds the triggers from clicking the buttons.
    this.__addTriggers();
    this.showScore();
  }
  showStatus(){
    let self = this;
    let statusIndicator = document.querySelectorAll("#status");
    statusIndicator.forEach(function(element) {
      element.textContent = self.status;
    });
  }
  newGame(){
    if(this.status == this.statusEnum.OFF){
      this.colorSeq = [];
      this.onNum = 0;
      this.addColor();
      this.showSeq();
      let matches = document.querySelectorAll("#GameState");
      matches.forEach(function(element) {
        element.disabled = true;
        element.textContent = "In-Game";
      });
      this.showStatus();
      matches = document.querySelectorAll(".color");
      matches.forEach(function(element) {
        element.classList.remove('disabled');
      });
    }
  }
  addColor(){
    // this allows for a arbatrary length of items.
    let color =  this.colorsPointers[Math.floor(Math.random() * this.colorsPointers.length)];
    // then we push it to the array.
    this.colorSeq.push(color);
  }
  showSeq(){
    this.status = this.statusEnum.WATCH;
    this.showStatus();
    let self = this;
    for(let seqID in this.colorSeq){
      let delay = (seqID * 1000) + 2;
      // set's a delay
      setTimeout(
        // the functuon we will run
        function(){ self.showColor(self.colorSeq[seqID]);},
        // when we will run it (id * 1 second, starts at 0)
        delay);
      delay -= 100;
      // set's a delay
      setTimeout(
        // the functuon we will run
        function(){ self.hideColor();},
        // when we will run it (id * 1 second, starts at 0)
        delay);
    }
    let delay = (this.colorSeq.length * 1000) + 2;
    setTimeout(
      // the functuon we will run
      function(){
        self.hideColor();
        self.status = self.statusEnum.REPEAT;
        self.showStatus();
        },
      // when we will run it (id * 1 second, starts at 0)
      delay);
  }
  playTone(toneVal){
    if(!(this.playing)){
      let context = new AudioContext()
      let tone = context.createOscillator()
      let gain = context.createGain()
      tone.type = "triangle";
      tone.frequency.value = toneVal;
      tone.connect(gain)
      gain.connect(context.destination)
      tone.start(0);
      this.tone = {gain, tone, context};
      this.playing = true;
    }
  }
  stopTone(){
    if(this.playing){
      let tone = this.tone;
      tone.gain.gain.exponentialRampToValueAtTime(
        0.000000001, tone.context.currentTime + 0.04
      );
      setTimeout(
        function(){ tone.tone.stop();},
        500);
      this.tone = null;
      this.playing = false;
    }
  }
  showColor(color){
    this.hideColor();
    let matches = document.querySelectorAll(color.css);
    matches.forEach(function(element) {
      element.classList.add('active');
    });
    this.playTone(color.tone);
    return;
  }
  hideColor(){
    let matches = document.querySelectorAll(".active");
    matches.forEach(function(element) {
      element.classList.remove('active');
    });
    this.stopTone();
    return;
  }
  __addTriggers(){
    let self = this;
    let matches = document.querySelectorAll(".color");
    matches.forEach(function(element) {
      element.addEventListener('click', function(e){self.onClick(e);});
    });
    matches = document.querySelectorAll("#GameState");
    matches.forEach(function(element) {
      element.addEventListener('click', function(e){self.newGame(e);});
    });
  }
  showScore(){
    let self = this;
    let matches = document.querySelectorAll("#HighScore");
    matches.forEach(function(element) {
      element.textContent = self.highScore;
    });
  }
  onClick(e){
    if(this.status == this.statusEnum.REPEAT){
      if (e.target.id == this.colorSeq[this.onNum].target){
        let self = this;
        this.playTone(this.colorSeq[this.onNum].tone);
        setTimeout(
          function(){ self.stopTone(); },
          250);
        this.onNum ++;
        if(this.onNum >= this.colorSeq.length){
          if(this.highScore < this.colorSeq.length){
            this.highScore = this.colorSeq.length;
          }
          this.showScore();
          // reset the on-num counter.
          this.onNum = 0;
          this.addColor();
          this.status = this.statusEnum.CORRECT;
          this.showStatus();
          setTimeout(
            function(){ self.showSeq();},
            750);
        }
      } else {
        this.status = this.statusEnum.OFF;
        let matches = document.querySelectorAll("#GameState");
        matches.forEach(function(element) {
          element.disabled = false;
          element.textContent = "Play again?";
        });
        this.showStatus();
        matches = document.querySelectorAll(".color");
        matches.forEach(function(element) {
          element.classList.add('disabled');
        });
      }
    }
  }
}

var testGame;

window.addEventListener('load', function() {
  testGame = new simonGame();
});
